// simple_int_set.cc

#include <iostream>
#include <string>
#include <vector>
#include "simple_int_set.h"
#include <stdlib.h>

using namespace std;

set<int> SetIntersection(const set<int>& set0,
                              const std::set<int>& set1) {
  set<int> set2;
  for( set<int>::iterator It = set1.begin();It != set1.end();++It ) {
    set<int>::iterator F = set0.find(*It);
    if( F != set0.end() )
    {
      set2.insert(*It); 
    }
  }
  return set2;
}

set<int> SetUnion(const set<int>& set0,
                       const set<int>& set1) {
  set<int> set2;
  for( set<int>::iterator It = set0.begin();It != set0.end();++It ) {
    set2.insert(*It); 
  }
  for( set<int>::iterator It = set1.begin();It != set1.end();++It ) {
    set<int>::iterator F = set0.find(*It);
    if( F == set0.end() )
    {
      set2.insert(*It); 
    }
  }
  return set2;
}

set<int> SetDifference(const set<int>& set0,
                            const set<int>& set1){

  set<int> set2;
  for( set<int>::iterator It = set0.begin();It != set0.end();++It ) {
    set<int>::iterator F = set1.find(*It);
    if( F == set1.end() )
    {
      set2.insert(*It); 
    }
  }
  return set2;
}

inline bool IsInt(const std::string& str) {
  bool ret = false;
  for (int i = 0; ret == false && i < str.size(); ++i) {
    ret = ('0' <= str[i] && str[i] <= '9');
  }
  return ret;
}


bool InputSet(std::istream& in, std::set<int>* s) {
  string str;
  in >> str;
  if (str != "{") return false;
  s->clear();
  while (true) {
    cin >> str;
    if (IsInt(str)) s->insert(atoi(str.c_str()));
    else if (str == "}") return true;
    else return false;
  }
}

void OutputSet(std::ostream& out, const std::set<int>& s){
  cout<<"{ ";
  for( set<int>::iterator It = s.begin();It != s.end();++It ) {
    cout<<*It<<" "; 
  }
  cout<<"}";
}