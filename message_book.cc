#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "message_book.h"

using namespace std;

MessageBook::MessageBook() {

}

MessageBook::~MessageBook() {

}

void MessageBook::AddMessage(int number, const std::string& message) {
	messages_[number]=message;
}

void MessageBook::DeleteMessage(int number) {
	 messages_.erase(number);
}

vector<int> MessageBook::GetNumbers() const{
	vector<int> ms;
	int k=0;
	map<int,string>::const_iterator it;
	for( it = messages_.begin(); it != messages_.end(); ++it )  {
		ms.resize(k+1);
		ms[k]=it->first;
		k++;
	}
	return ms;
}

const string& MessageBook::GetMessage(int number) const{
	map<int,string>::const_iterator it;
	for( it = messages_.begin(); it != messages_.end(); ++it ) {
		if(it->first==number) return it->second;
	}
}

