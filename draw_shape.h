// draw_shape.h

#ifndef _DRAW_SHAPE_H_
#define _DRAW_SHAPE_H_

#include <iostream>
#include <vector>

enum { RECTANGLE, TRIANGLE_UP, TRIANGLE_DOWN };
enum { ERROR_OUT_OF_CANVAS = -1, ERROR_INVALID_INPUT = -2 };

struct Shape {
  int type;
  int x, y;
  int width, height;
  char brush;  // The character to draw the shape.
};

class Canvas {
 public:
  Canvas(size_t row, size_t col);
  ~Canvas();

  int AddShape(const Shape &s);  // Return the index of the shape.
  void DeleteShape(int index);
  void Draw(std::ostream& os);
  void Dump(std::ostream& os);

 private:
  size_t width_, height_; 
  std::vector<Shape> shapes_;
};

#endif  // _DRAW_SHAPE_H_
