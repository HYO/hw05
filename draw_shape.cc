// draw_shape.cc

#include <iostream>
#include <vector>
#include <string>
#include "draw_shape.h"

using namespace std;

int i=0;

Canvas::Canvas (size_t row, size_t col) {
	width_=row;
	height_=col;
}
Canvas::~Canvas () {
	width_=0;
	height_=0;
}

int Canvas::AddShape(const Shape &s) {
	if(s.type==RECTANGLE&&s.x+((s.width-1)/2)>=width_)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==RECTANGLE&&s.x-((s.width-1)/2)<0)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==RECTANGLE&&s.y+((s.height-1)/2)>=height_)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==RECTANGLE&&s.y-((s.height-1)/2)<0)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==TRIANGLE_DOWN&&s.x+s.height-1>=width_)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==TRIANGLE_DOWN&&s.x-s.height+1<0)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==TRIANGLE_DOWN&&s.y-s.height+1<0)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==TRIANGLE_UP&&s.x+s.height-1>=width_)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==TRIANGLE_UP&&s.x-s.height+1<=0)
		return ERROR_OUT_OF_CANVAS;
	if(s.type==TRIANGLE_UP&&s.y+s.height-1>=height_)
		return ERROR_OUT_OF_CANVAS;
	if(s.width%2==0||s.height%2==0)
		return ERROR_INVALID_INPUT;
	shapes_.resize(i+1);
	shapes_[i].type=s.type;
	shapes_[i].x=s.x;
	shapes_[i].y=s.y;
	shapes_[i].height=s.height;
	if(s.type==RECTANGLE)
		shapes_[i].width=s.width;
	else 
		shapes_[i].width=s.height;
	shapes_[i].brush=s.brush;
	i++;
	return 0;
	
}// Return the index of the shape.
void Canvas:: DeleteShape(int index) {
	for(int k=index; k<i-1; k++) {
		shapes_[k].type=shapes_[k+1].type;
		shapes_[k].x=shapes_[k+1].x;
		shapes_[k].y=shapes_[k+1].y;
		shapes_[k].height=shapes_[k+1].height;
		shapes_[k].width=shapes_[k+1].width;
		shapes_[k].brush=shapes_[k+1].brush;
	}
	if(index<i)i=i-1;
}
void Canvas::Draw(std::ostream& os) {
	cout<<" ";
	for(int k=0; k<width_;k++ ) cout<<k;
	cout<<endl;
	for(int k=0; k<height_;k++) {
		cout<<k;
		for( int j=0; j<width_;j++) {
			int p=1;
			for (int m=i-1; m>=0; m--) {
				if(shapes_[m].type==RECTANGLE) {
					if(shapes_[m].x-((shapes_[m].width-1)/2)<=j&& shapes_[m].x+((shapes_[m].width-1)/2)>=j)
						if(shapes_[m].y-((shapes_[m].height-1)/2)<=k&& shapes_[m].y+((shapes_[m].height-1)/2)>=k) {
							cout<<shapes_[m].brush;p=0;break;}
				}
				else if (shapes_[m].type==TRIANGLE_DOWN) {
					if(shapes_[m].y-shapes_[m].width+1<=k&&shapes_[m].y>=k)
						if(j+k<=shapes_[m].x+shapes_[m].y&&j-k>=shapes_[m].x-shapes_[m].y) {
							cout<<shapes_[m].brush;p=0;break;}
				}
				else if (shapes_[m].type==TRIANGLE_UP) {
					if(shapes_[m].y+shapes_[m].width-1>=k&&shapes_[m].y<=k)
						if(j-k<=shapes_[m].x-shapes_[m].y&&j+k>=shapes_[m].x+shapes_[m].y){
							cout<<shapes_[m].brush;p=0;break;}
				}
			}
			if(p==1) cout<<".";
		}
		cout<<endl;
	}

}
void Canvas::Dump(std::ostream& os) {
	for(int k=0; k<i; k++) {
		cout<<k<<" ";
		if(shapes_[k].type==RECTANGLE) cout<<"rect ";
		if(shapes_[k].type==TRIANGLE_DOWN) cout<<"tri_down ";
		if(shapes_[k].type==TRIANGLE_UP) cout<<"tri_up ";
		cout<<shapes_[k].x<<" "<<shapes_[k].y<<" "<<shapes_[k].width;
		if(shapes_[k].type==RECTANGLE) cout<<" "<<shapes_[k].height;
		cout<<" "<<shapes_[k].brush<<endl;
	}
}