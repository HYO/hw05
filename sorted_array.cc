#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "sorted_array.h"

using namespace std;

SortedArray::SortedArray() {
	
}
SortedArray::~SortedArray() {
	numbers_.resize(0);
}

void SortedArray::AddNumber(int num) {
	numbers_.resize(numbers_.size()+1);
	numbers_[numbers_.size()-1]=num;
	sort(numbers_.begin(),numbers_.end());
}

vector<int> SortedArray::GetSortedAscending() const {
	return numbers_;
}

vector<int> SortedArray::GetSortedDescending() const {
	vector<int> num;
	int k=numbers_.size();
	num.resize(k);
	for(int i=0;i<numbers_.size(); i++) {
		num[i]=numbers_[k-i-1];
	}
	return num;
}
int SortedArray::GetMax() const {
	return numbers_[numbers_.size()-1];
}

int SortedArray::GetMin() const {
	return numbers_[0];
}